﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TaskManager.Models
{
    public class TaskModel
    {
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Ввод")]
        public string In { get; set; }

        [Display(Name = "Вывод")]
        public string Out { get; set; }
    }
}
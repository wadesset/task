﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using TaskManager.Models;

namespace TaskManager.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [Authorize]
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.RoleList = GetRoles(UserManager);
            List<TaskModel> taskList = new List<TaskModel>();
            List<IdentityRole> roleList = GetRoles(UserManager);

            foreach (var role in roleList)
            {
                taskList.Add(new TaskModel() { Name = role.Name });
            }
            return View(taskList);
        }


        [Authorize]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        /// <summary>
        /// Создает и запускает в работу новую задачу
        /// </summary>
        /// <param name="name">Имя задачи</param>
        /// <param name="in">Входной параметр</param>
        /// <returns>Результат задачи</returns>
        private TaskModel CreateTask(string name, string @in)
        {
            TaskManager taskManager = new TaskManager();
            return new TaskModel { Name = name, In = @in, Out = taskManager.Work(name, @in)};
        }

        [Authorize]
        [HttpPost]
        public ActionResult Index(string name, string dynamicField)
        {
            TaskModel task = CreateTask(name, dynamicField);
            List<TaskModel> taskList = new List<TaskModel>();
            List<IdentityRole> roleList = GetRoles(UserManager);

            foreach (var role in roleList)
            {
                taskList.Add(role.Name == name ? task : new TaskModel() {Name = role.Name});
            }

            return View(taskList);
        }


        /// <summary>
        /// Получение всех ролей пользователя, кроме административных
        /// </summary>
        /// <param name="userManager">Пользователь</param>
        /// <returns>Список всех ролей пользователя</returns>
        private List<IdentityRole> GetRoles(ApplicationUserManager userManager)
        {
            var user = userManager.FindByIdAsync(User.Identity.GetUserId());
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            List<IdentityRole> roleList = new List<IdentityRole>();

            foreach (var role in user.Result.Roles)
            {
                var r = roleManager.FindByIdAsync(role.RoleId).Result;
                if (r.Name != "administrator" && r.Name != "user")
                    roleList.Add(r);
            }

            return roleList;
        }
    }
}
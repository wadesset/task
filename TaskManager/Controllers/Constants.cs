﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManager.Controllers
{
    /// <summary>
    /// Постоянные значения. Имена задач
    /// </summary>
    public static class Constants
    {
        public const string TaskA = "a";
        public const string TaskB = "b";
        public const string TaskC = "c";
        public const string TaskD = "d";
        public const string TaskE = "e";
        public const string TaskF = "f";
        public const string TaskG = "g";
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaskManager.Controllers
{
    /// <summary>
    /// Менеджер задач
    /// </summary>
    public class TaskManager
    {
        private delegate string Task(string @in);
        
        /// <summary>
        /// Конструктор. Пока что пустой
        /// </summary>
        public TaskManager()
        {
        }

        /// <summary>
        /// Менеджер задач
        /// </summary>
        /// <param name="taskName">Имя задачи</param>
        /// <param name="in">Входной параметр</param>
        /// <returns>Результат выполненной задачи</returns>
        public string Work(string taskName, string @in)
        {
            Dictionary<string, Delegate> dict = new Dictionary<string, Delegate>();
            dict.Add(Constants.TaskA, new Task(TaskA));
            dict.Add(Constants.TaskB, new Task(TaskB));
            dict.Add(Constants.TaskC, new Task(TaskC));
            dict.Add(Constants.TaskD, new Task(TaskD));

            foreach (var o in dict.Where(f => f.Key == taskName))
            {
                return o.Value.DynamicInvoke(@in).ToString();
            }

            return null;
        }

        /// <summary>
        /// Метод задачи. Переводит в двоичную систему
        /// </summary>
        /// <param name="in">Входной параметр</param>
        /// <returns>Чсло в двоичной системе</returns>
        private string TaskA(string @in)
        {
            if (!BadValidate(@in))
                return "Введите число";
            var t = Convert.ToInt32(@in);
           return Convert.ToString(t, 2);
        }

        /// <summary>
        /// Метод задачи. Переводит в шестнадцатеричную систему
        /// </summary>
        /// <param name="in">Входной параметр</param>
        /// <returns>Число в шестнадцатеричной системе</returns>
        private string TaskB(string @in)
        {
            if (!BadValidate(@in))
                return "Введите число";
            var t = Convert.ToInt32(@in);
            return Convert.ToString(t, 16);
        }

        /// <summary>
        /// Метод задачи. Переворачивает строку
        /// </summary>
        /// <param name="in">Входной параметр</param>
        /// <returns>Перевернутая строка</returns>
        private string TaskC(string @in)
        {
            return new string(@in.ToCharArray().Reverse().ToArray());
        }

        /// <summary>
        /// Метод задачи. Создание массива чисел Фибоначчи из входного параметра.
        /// </summary>
        /// <param name="in">Входной параметр</param>
        /// <returns></returns>
        private string TaskD(string @in)
        {
            if (!BadValidate(@in))
                return "Введите число";
            return EvenSumm(GetFib(Convert.ToInt32(@in))).ToString();
        }

        /// <summary>
        /// Хелпер задачи. Получает ряд Фибоначчи
        /// </summary>
        /// <param name="value">Количество чисел</param>
        /// <returns></returns>
        private List<int> GetFib(int value)
        {
            var fibList = new List<int>();
            var d = 1;
            for (var i = 1; i <= value; i += d)
            {
                fibList.Add(i);
                d = i - d;
            }

            return fibList;
        }

        /// <summary>
        /// Хелпер задачи. Вычисляет сумму четных чисел в коллекции
        /// </summary>
        /// <param name="value">Коллекция чисел</param>
        /// <returns>Сумма четных чисел</returns>
        private int EvenSumm(List<int> value)
        {
            int result = 0;
            foreach (var v in value)
            {
                if (v % 2 != 0)
                    result += v;
            }

            return result;
        }

        /// <summary>
        /// Плохой минимальный вариант валидации
        /// </summary>
        /// <param name="value">Проверяемое значение</param>
        /// <returns>Результат</returns>
        public bool BadValidate(string value)
        {
            try
            {
                Convert.ToInt32(value);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }
    }
}